pnetcdf (1.12.3-1) unstable; urgency=medium

  * Standards-Version: 4.6.0
  * New upstream release
  * Drop -pedantic flag building F90 code; breaks bindings 

 -- Alastair McKinstry <mckinstry@debian.org>  Sat, 26 Feb 2022 19:36:18 +0000

pnetcdf (1.12.2-1) unstable; urgency=medium

  * New upstream release
  * Standards-Version: 4.5.1
  * Point vcs-git to debian/latest branch
  * Ensure DEB_MAINT_* settings are exported. Closes: #901838

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 21 Jan 2021 10:22:12 +0000

pnetcdf (1.12.1-3) unstable; urgency=medium

  * On test failure, record details but don't fail.
  * Run tests on both static and shared library builds

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 01 Sep 2020 09:15:02 +0100

pnetcdf (1.12.1-2) unstable; urgency=medium

  * Ignore argument-mismatch warnings in Fortran/C interface with gfortran 10;
    they're false positives due to voidptrs. Closes: #957692
  * Debhelper version 13
  * Standards-Version: 4.5.0

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 23 Jul 2020 11:11:46 +0100

pnetcdf (1.12.1-1) unstable; urgency=medium

  * New upstream release

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 13 Dec 2019 13:44:44 +0000

pnetcdf (1.12.0-1) unstable; urgency=medium

  * New upstream release
  * Undo hard-coded gfortran-8
  * Standards-Version: 4.4.1.0

 -- Alastair McKinstry <mckinstry@debian.org>  Wed, 02 Oct 2019 20:06:53 +0100

pnetcdf (1.11.2-2) unstable; urgency=medium

  * Update watch file
  * Standards-Version: 4.4.0
  * Use debhelper-compat (=12)

 -- Alastair McKinstry <mckinstry@debian.org>  Sat, 03 Aug 2019 08:18:18 +0100

pnetcdf (1.11.2-1) unstable; urgency=medium

  * New upstream release

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 18 Jun 2019 17:12:56 +0100

pnetcdf (1.11.1-1) unstable; urgency=medium

  * New upstream release
  * B-D on gfortran | fortran-compiler
  * Now use debhelper 12
  * docs: now multiple READMEs instead of .txt files shipped

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 19 Apr 2019 07:50:19 +0100

pnetcdf (1.11.0-1) experimental; urgency=medium

  * New upstream release
  * Standards-Version: 4.3.0

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 11 Jan 2019 10:49:01 +0000

pnetcdf (1.11.0~pre1-1) experimental; urgency=medium

  * New upstream release

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 18 Dec 2018 08:03:28 +0000

pnetcdf (1.10.0-2) unstable; urgency=medium

  * Patch from Gianfranco Costamagna to avoid FTBFS of reverse-deps
    by forcing libking of gfortran. Closes: #916728

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 18 Dec 2018 07:22:58 +0000

pnetcdf (1.10.0-1) unstable; urgency=medium

  * New upstream release
  * Remove obsolete patches:
    - hardening.patch
    - destdir.patch
  * Standards-Version: 4.2.1
  * Undo hard-coded gfortran; transition complete
  * Update watch file to github
  * Don't use hard-coded xz compression

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 12 Nov 2018 14:30:59 +0000

pnetcdf (1.9.0-8) unstable; urgency=medium

  * Link libpnetcdf0d against libgfortran

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 09 Jul 2018 06:57:33 +0100

pnetcdf (1.9.0-7) unstable; urgency=medium

  * Transition to gfortran-8. Needed for netcdf-parallel build

 -- Alastair McKinstry <mckinstry@debian.org>  Wed, 04 Jul 2018 19:56:43 +0100

pnetcdf (1.9.0-6) unstable; urgency=medium

  * Disable erange_fill test that fails incorrectly on S390x.
    Closes: #899319

 -- Alastair McKinstry <mckinstry@debian.org>  Wed, 23 May 2018 09:37:17 +0100

pnetcdf (1.9.0-5) unstable; urgency=medium

  * Undo gfortran-8 changes
  * Drop -lgfortran; now breaking non-x86 builds
  * Add -g to flags to ensure debug symbols are added
  * Ensure no rpaths in binaries

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 21 May 2018 15:25:28 +0100

pnetcdf (1.9.0-4) unstable; urgency=medium

  * Use dh-dfortran-mod to add dependency on fortran compiler
  * Use B-D of gfortran-8 to enforce the transition
  * Add tests: call test suite in build-shared

 -- Alastair McKinstry <mckinstry@debian.org>  Sat, 05 May 2018 14:30:03 +0100

pnetcdf (1.9.0-3) unstable; urgency=medium

  * Rebuilt for openmpi transition
  * Add riscv64 as architecture
  * Standards-Version: 4.1.4

 -- Alastair McKinstry <mckinstry@debian.org>  Sun, 15 Apr 2018 08:00:28 +0100

pnetcdf (1.9.0-2) unstable; urgency=medium

  * Only build on 64-bit archs. Needed as MPI_OFFSET must now be 8 bytes,
    and the same size as size_t. Closes: #889115

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 08 Feb 2018 11:40:19 +0000

pnetcdf (1.9.0-1) unstable; urgency=medium

  * New upstream release
  * Standards-Version: 4.1.3, no changes required
  * Add salsa.debian.org as VCS
  * Add watch file

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 01 Feb 2018 09:32:52 +0000

pnetcdf (1.8.1-3) unstable; urgency=medium

  * Standards-Version: 4.1.1, no changes required
  * Add gpb.conf file
  * Drop obsolete build-depends on auto*

 -- Alastair McKinstry <mckinstry@debian.org>  Wed, 25 Oct 2017 15:36:01 +0100

pnetcdf (1.8.1-2) unstable; urgency=medium

  * Push to unstable

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 19 Jun 2017 05:42:15 +0100

pnetcdf (1.8.1-1) experimental; urgency=medium

  * New upstream release
   - pkgconfig now supplied upstream

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 20 Apr 2017 13:59:23 +0100

pnetcdf (1.7.0-1) unstable; urgency=medium

  * New upstream release.
  * Add dependency on libpnetcdf0d to -dev package.
  * Add pkgconfig support
  * Make libpnetcdf0d properly Multi-Arch. Closes: #815821.
  * Put inc, mod files in /usr/include/$(ARCH) as not
    arch-independent
  * Ensure no rpath in shared lib.
  * Move to Standards-Version: 3.9.8
  * Now use debhelper 10

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 14 Jun 2016 16:04:59 +0100

pnetcdf (1.7.0~pre1-1) unstable; urgency=medium

  * Initial release. (Closes: #812187)
    Re-submitted with updated Copyright Information.

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 16 Feb 2016 23:59:30 +0000
